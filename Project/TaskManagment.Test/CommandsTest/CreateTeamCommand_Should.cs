﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;


namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class CreateTeamCommand_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
        }
        [TestMethod]
        public void CreateTeamCommandTest_Should_Throw_When_NumberOfArgumentsAreNotCorrect()
        {
            //Arrange
            ICommand createTeam = commandFactory.Create("CreateTeam");
            //Act

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createTeam.Execute());
        }
        [TestMethod]
        public void CreateTeamCommandTest_Should_Throw_When_TeamAlreadyExists()
        {
            //Arrange
            ICommand createTeam = commandFactory.Create("Createteam Piletata");
            createTeam.Execute();
            ICommand createTeam1 = commandFactory.Create("Createteam Piletata");
            //Act

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createTeam1.Execute());
        }
        [TestMethod]
        public void CreateTeamCommandTest_Should_CreateTeam_When_CorrectInput()
        {
            //Arrange
            ICommand createTeam = commandFactory.Create("Createteam Piletata");
            
            //Act

            //Assert
            Assert.AreEqual("Team with name:Piletata was created!", createTeam.Execute());
        }
    }
}
