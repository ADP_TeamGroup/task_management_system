﻿using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models.Validations;

namespace Task_Management.Models
{
    public class Story : Task, IStory
    {
        public Story(int id, string title, string description, PriorityType priority, SizeStory size, StoryStatus status)
            : base(id, title, description)
        {
            this.Priority = priority;
            this.Size = size;
            this.Status = status;

            var storyHistory = new ActivityHistory($"Story with id {this.ID} was created!");
            this.AddHistory(storyHistory);
        }

        public override string Title
        {
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Title", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length,
                                                       Constants.StoryTitleMinLength,
                                                       Constants.StoryTitleMaxLength,
                                                       this.GetType().Name,
                                                       "title");
                base.Title = value;
            }
        }

        public override string Description
        {
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Description", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length,
                                                       Constants.StoryDescrMinLength,
                                                       Constants.StoryDescrMaxLength,
                                                       this.GetType().Name,
                                                       "description");
                base.Description = value;
            }
        }

        public PriorityType Priority { get; set; }

        public SizeStory Size { get; set; }

        public StoryStatus Status { get; set; }

    }
}
