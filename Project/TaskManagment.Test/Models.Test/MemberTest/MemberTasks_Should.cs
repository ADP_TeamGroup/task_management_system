﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;

namespace TaskManagment.Test.Models.Test.MemberTest
{
    [TestClass]
    public class MemberTasks_Should
    {
        [TestMethod]
        public void MemberTasks_Should_ReturnCopy_When_TaskIsAddedViaMethod()
        {
            //Arrange
            IMember member = new Member("Gosho");
            ITask task = new Bug(5,"Fix that bug mate", "Can not load in the matrix", PriorityType.Medium, SeverityType.Critical, "Open Vs", "Go to the tools");
            //Act
            member.AddTasksToMember(task);
            //Assert
            Assert.AreEqual(1, member.Tasks.Count);
            Assert.AreEqual(task, member.Tasks[0]);
        }
        [TestMethod]
        public void MemberTasks_Should_ReturnCopy_When_TaskIsAddedThroughtProperty()
        {
            //Arrange
            IMember member = new Member("Gosho");
            ITask task = new Bug(5,"Fix that bug mate", "Can not load in the matrix", PriorityType.Medium, SeverityType.Critical, "Open Vs", "Go to the tools");
            //Act
            member.Tasks.Add(task);
            //Assert
            Assert.AreEqual(0, member.Tasks.Count);
            
        }
    }
}
