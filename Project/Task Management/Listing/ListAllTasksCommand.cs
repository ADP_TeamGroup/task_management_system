﻿using System.Text;
using Task_Management.Commands;
using Task_Management.Core.Contracts;
using System.Linq;
using System.Collections.Generic;
using Task_Management.Exceptions;

namespace Task_Management.Listing
{
    public class ListAllTasksCommand : BaseCommand
    {
        public ListAllTasksCommand(IList<string> parameters, IRepository repository)
            : base(parameters, repository) { }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                string errorMassage = string.Format(Constants.NumberOFArgumentsErrorMsg, 1, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMassage);
            }

            string parameterToFilter = this.CommandParameters[0];

            return ListAllTasksWithTwoParameters(parameterToFilter);
        }

        private string ListAllTasksWithTwoParameters(string parameterToFilter)
        {
            var list = this.Repository.Tasks.Where(t=> t.Title.Contains(parameterToFilter)).ToList();

            if (list.Count == 0)
            {
                throw new InvalidUserInputException($"There isn't any task which contains {parameterToFilter} in title!");
            }

            var sbTasks = new StringBuilder();

            foreach (var task in list.OrderBy(t=> t.Title))
            {
                sbTasks.AppendLine($"ID: {task.ID} : '{task.Title}'");
            }

            return sbTasks.ToString().Trim();
        }
    }
}
