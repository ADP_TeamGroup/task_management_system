﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Listing;

namespace Task_Management.Core
{
    public class CommandFactory : ICommandFactory
    {
        private const char SplitCommandSymbol = ' ';
        private const string CommentOpenSymbol = "{{";
        private const string CommentCloseSymbol = "}}";
        private const char TaskSymbol = '"';
        private readonly IRepository repository;

        public CommandFactory(IRepository repository)
        {
            this.repository = repository;
        }

        public ICommand Create(string commandLine)
        {
            string commandName = ExtractCommandName(commandLine);
            List<string> commandParameters = ExtractCommandParameters(commandLine);

            ICommand command;

            switch (commandName)
            {
                case "addmember":
                    command = new AddMemberToTeamCommand(commandParameters, repository);
                    break;
                case "addcomment":
                    command = new AddCommentToTaskCommand(commandParameters, repository);
                    break;
                case "createteam":
                    command = new CreateTeamCommand(commandParameters, repository);
                    break;
                case "createperson":
                    command = new CreatePersonCommand(commandParameters, repository);
                    break;
                case "createboard":
                    command = new CreateBoardCommand(commandParameters, repository);
                    break;
                case "createbug":
                    command = new CreateBugInBoardCommand(commandParameters, repository);
                    break;
                case "createstory":
                    command = new CreateStoryInBoardCommand(commandParameters, repository);
                    break;
                case "createfeedback":
                    command = new CreateFeedbackInBoardCommand(commandParameters, repository);
                    break;
                case "assigntask":
                    command = new AssignTaskToMember(commandParameters, repository);
                    break;
                case "unassigntask":
                    command = new UnnasignTaskOfMember(commandParameters, repository);
                    break;
                case "showallpeople":
                    command = new ShowAllPeopleCommand(repository);
                    break;
                case "showallteammembers":
                    command = new ShowAllTeamMembersCommand(commandParameters, repository);
                    break;
                case "showallteams":
                    command = new ShowAllTeamsCommand(repository);
                    break;
                case "showallteamboards":
                    command = new ShowAllBoardsInTeamCommand(commandParameters, repository);
                    break;
                case "change":
                    command = new ChangeCommand(commandParameters, repository);
                    break;
                case "listalltasks":
                    command = new ListAllTasksCommand(commandParameters, repository);
                    break;
                case "listbugs":
                    command = new ListBugsOnly(commandParameters, repository);
                    break;
                default:
                    throw new InvalidUserInputException("Command with such name doesnot exist");

            }

            return command;
        }
        private string ExtractCommandName(string commandLine)
        {
            string commandName = commandLine.Split(SplitCommandSymbol)[0].ToLower();
            return commandName;
        }
        private List<string> ExtractCommandParameters(string commandLine)
        {
            List<string> parameters =new List<string>();
            List<string> commands = new List<string>(commandLine.Split(SplitCommandSymbol));
            commands.RemoveAt(0);
            
            for(int i = 0; i < commands.Count; i++)
            {
                if (commands[i].StartsWith(CommentOpenSymbol))
                {
                    StringBuilder comment = new StringBuilder();
                    string firstPartOfTheComment = commands[i].Trim('{');
                    commands[i] = firstPartOfTheComment;
                    while (!commands[i].EndsWith(CommentCloseSymbol))
                    {
                        comment.Append(commands[i]  + " ");
                        i++;
                        if (i == commands.Count)
                        {
                            throw new InvalidUserInputException("The comment is open but never closed!");
                        }
                    }
                    string lastPartOfComment = commands[i].Trim('}');
                    comment.Append(lastPartOfComment);
                    parameters.Add(comment.ToString());
                }
                else if (commands[i].StartsWith(TaskSymbol))
                {
                    StringBuilder taskMembers = new StringBuilder();
                    string firstPartOfTaskMember = commands[i].Trim(TaskSymbol);
                    commands[i] = firstPartOfTaskMember;
                    while (!commands[i].EndsWith(TaskSymbol))
                    {
                        taskMembers.Append(commands[i] + " ");
                        i++;
                        if (i == commands.Count)
                        {
                            throw new InvalidUserInputException("The comment is open but never closed!");
                        }
                    }
                    string lastPartOfTaskMember = commands[i].Trim(TaskSymbol);
                    taskMembers.Append(lastPartOfTaskMember);
                    parameters.Add(taskMembers.ToString());
                }
                else
                {
                    parameters.Add(commands[i]);
                }
            }
            return parameters;
        }

        //private List<string> ExtractCommandParameters(string commandLine)
        //{
        //    List<string> parameters = new List<string>();


        //    //For comments
        //    var indexOfOpenComment = commandLine.IndexOf(CommentOpenSymbol);
        //    var indexOfCloseComment = commandLine.IndexOf(CommentCloseSymbol);

        //    if (indexOfOpenComment >= 0)
        //    {
        //        var commentStartIndex = indexOfOpenComment + CommentOpenSymbol.Length;
        //        var commentLength = indexOfCloseComment - CommentCloseSymbol.Length - indexOfOpenComment;
        //        string commentParameter = commandLine.Substring(commentStartIndex, commentLength);
        //        parameters.Add(commentParameter);

        //        Regex regex = new Regex("{{.+(?=}})}}");
        //        commandLine = regex.Replace(commandLine, string.Empty);
        //    }

        //    var indexOfFirstSeparator = commandLine.IndexOf(SplitCommandSymbol);

        //    if(indexOfFirstSeparator == -1)
        //    {
        //        return parameters;
        //    }

        //    parameters.AddRange(commandLine.Substring(indexOfFirstSeparator + 1).Split(new[] { SplitCommandSymbol }, StringSplitOptions.RemoveEmptyEntries));

        //    return parameters;
        //}
    }
}
