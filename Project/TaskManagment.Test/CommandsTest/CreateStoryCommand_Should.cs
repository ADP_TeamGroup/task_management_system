﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class CreateStoryCommand_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_TeamInNotValid()
        {
            //Arrange 
            string board = "Board";
            string title = "\"There is ne feature u need to implement\"";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            PriorityType priorityType = PriorityType.High;
            SizeStory size = SizeStory.Small;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory kotkite {board} {title} {description} {priorityType} {size} {storyStatus}");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_BoardInNotValid()
        {

            //Arrange 
            string team = "Teams";
            string title = "\"There is ne feature u need to implement\"";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            PriorityType priorityType = PriorityType.High;
            SizeStory size = SizeStory.Small;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory {team} Board2 {title} {description} {priorityType} {size} {storyStatus}");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_TitleInNotValid()
        {
            //Arrange 
            string team = "Teams";
            string board = "Board";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            PriorityType priorityType = PriorityType.High;
            SizeStory size = SizeStory.Small;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory {team} {board} \"There is ne feature u need to implement {description} {priorityType} {size} {storyStatus}");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_DescriptionFormatInNotValid()
        {
            //string title, string description, PriorityType priority, SizeStory size, StoryStatus status
            //Arrange 
            string team = "Teams";
            string board = "Board";
            string title = "\"There is ne feature u need to implement\"";
            PriorityType priorityType = PriorityType.High;
            SizeStory size = SizeStory.Small;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory {team} {board} {title} This new feature is new board function that the client will be able to play tetris when he waits to log in :D {priorityType} {size} {storyStatus}");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_PriorityTypeIsNotValid()
        {
            //string title, string description, PriorityType priority, SizeStory size, StoryStatus status
            //Arrange 
            string team = "Teams";
            string board = "Board";
            string title = "\"There is ne feature u need to implement\"";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            SizeStory size = SizeStory.Small;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory {team} {board} {title} {description} SuperHigh {size} {storyStatus}");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_SizeOfStoryTypeIsNotValid()
        {
            //string title, string description, PriorityType priority, SizeStory size, StoryStatus status
            //Arrange 
            string team = "Teams";
            string board = "Board";
            string title = "\"There is ne feature u need to implement\"";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            PriorityType priorityType = PriorityType.High;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory {team} {board} {title} {description} {priorityType} SuperLarge {storyStatus}");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Throw_When_StoryStatusTypeIsNotValid()
        {
            //string title, string description, PriorityType priority, SizeStory size, StoryStatus status
            //Arrange 
            string team = "Teams";
            string board = "Board";
            string title = "\"There is ne feature u need to implement\"";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            PriorityType priorityType = PriorityType.High;
            SizeStory size = SizeStory.Small;

            ICommand createStory = commandFactory.Create($"Createstory {team} {board} {title} {description} {priorityType} {size} Progress");

            Assert.ThrowsException<InvalidUserInputException>(() => createStory.Execute());
        }
        [TestMethod]
        public void CreateStoryCommand_Should_Return_When_InputIsValid()
        {
            //string title, string description, PriorityType priority, SizeStory size, StoryStatus status
            //Arrange 
            string team = "Teams";
            string board = "Board";
            string title = "\"There is ne feature u need to implement\"";
            string description = "\"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\"";
            PriorityType priorityType = PriorityType.High;
            SizeStory size = SizeStory.Small;
            StoryStatus storyStatus = StoryStatus.InProgress;

            ICommand createStory = commandFactory.Create($"Createstory {team} {board} {title} {description} {priorityType} {size} {storyStatus}");

            Assert.AreEqual("Story with Id:1 was created in board:Board in team:Teams!" ,createStory.Execute());
        }

    }
}
