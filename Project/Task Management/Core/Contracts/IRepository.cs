﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;

namespace Task_Management.Core.Contracts
{
    public interface IRepository
    {
        public IList<ITeam> Teams { get; }

        public IList<IMember> Persons { get; }

        public IList<ITask> Tasks { get; }

        public IList<IMember> MembersInTeam { get; }

        public IBug CreateBug(string teamName, string boardName, string title, string description
                        , PriorityType priority, SeverityType severity
                        , params string[] listOfStepsToReproduce);

        public IStory CreateStory(string teamName, string boardName, string title, string description, PriorityType priority, SizeStory size
                                , StoryStatus status);

        public IFeedback CreateFeedback(string teamName, string boardName, string title, string description, int rating, FeedbackStatus status);

        public ITeam CreateTeam(string name);

        public IBoard CreateBoard(string name, string teamName);

        public IMember CreatePerson(string personName);

        public IComment CreateComment(string commentMsg, string member);

        public void AddPersonToTeam(string name, string team);

        public ITeam FindTeam(string teamName);

        public IMember FindPerson(string personName);

        public ITask FindTask(int id);

        public bool PersonExists(string name);

        public bool MemberExistInTeam(string personName, string teamName);

        public bool TeamExists(string teamName);

        public bool BoardExists(string boardName, string teamName);

        public bool TaskExist(int id);

        public void AssignTaskToPerson(string personName, int taskID);

        public void UnassignTaskOfPerson(string personName, int taskId);

    }
}
