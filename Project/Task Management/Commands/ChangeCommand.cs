﻿using System.Collections.Generic;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;

namespace Task_Management.Commands
{
    public class ChangeCommand : BaseCommand
    {
        public ChangeCommand(IList<string> parameters, IRepository repository)
            : base(parameters, repository) { }

        public override string Execute()
        {
            if (CommandParameters.Count != 3)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, 3, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMsg);
            }

            int id = ParseIntParameter(this.CommandParameters[0], "ID");

            if (!this.Repository.TaskExist(id))
            {
                throw new InvalidUserInputException($"Task with ID:{id} doesn't exist!");
            }

            string typeOfParameter = this.CommandParameters[1];

            return ChangeParametersOfID(id, typeOfParameter);
        }

        private string ChangeParametersOfID(int id, string typeOfParameter)
        {
            var task = this.Repository.FindTask(id);
            string changeCommand = this.CommandParameters[2];
            string statusFrom = string.Empty;

            if (task is IBug)
            {
                Bug bug = (Bug)task;

                switch (typeOfParameter.ToLower())
                {
                    case "priority":
                        statusFrom = bug.Priority.ToString();
                        bug.Priority = ParseEnumParameter<PriorityType>(changeCommand, "priority");
                        break;
                    case "severity":
                        statusFrom = bug.Severity.ToString();
                        bug.Severity = ParseEnumParameter<SeverityType>(changeCommand, "severity");
                        break;
                    case "status":
                        statusFrom = bug.Status.ToString();
                        bug.Status = ParseEnumParameter<BugStatus>(changeCommand, "status");
                        break;
                    default:
                        throw new InvalidUserInputException($"{bug.GetType().Name} doesn't have {typeOfParameter}!");
                }
            }
            else if (task is IStory)
            {
                Story story = (Story)task;

                switch (typeOfParameter.ToLower())
                {
                    case "priority":
                        statusFrom = story.Priority.ToString();
                        story.Priority = ParseEnumParameter<PriorityType>(changeCommand, "priority");
                        break;
                    case "size":
                        statusFrom = story.Size.ToString();
                        story.Size = ParseEnumParameter<SizeStory>(changeCommand, "size");
                        break;
                    case "status":
                        statusFrom = story.Status.ToString();
                        story.Status = ParseEnumParameter<StoryStatus>(changeCommand, "status");
                        break;
                    default:
                        throw new InvalidUserInputException($"{story.GetType().Name} doesn't have {typeOfParameter}!");
                }
            }
            else if (task is IFeedback)
            {
                Feedback feedback = (Feedback)task;

                switch (typeOfParameter.ToLower())
                {
                    case "rating":
                        statusFrom = feedback.Rating.ToString();
                        feedback.Rating = ParseIntParameter(changeCommand, "rating");
                        break;
                    case "status":
                        statusFrom = feedback.Status.ToString();
                        feedback.Status = ParseEnumParameter<FeedbackStatus>(changeCommand, "status");
                        break;
                    default:
                        throw new InvalidUserInputException($"{feedback.GetType().Name} doesn't have {typeOfParameter}!");

                }
            }
            else
            {
                throw new InvalidUserInputException("Type is not valid");
            }

            return $"{typeOfParameter} of task with ID:{id} is changed from:{statusFrom} to :{changeCommand}.";
        }
    }
}
