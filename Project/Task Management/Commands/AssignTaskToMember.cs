﻿using System.Collections.Generic;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class AssignTaskToMember : BaseCommand
    {
        public AssignTaskToMember(IList<string> parameters, IRepository repository)
            : base(parameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 2)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, 2, this.CommandParameters.Count);

                throw new InvalidUserInputException(errorMsg);
            }

            string personName = this.CommandParameters[0];

            if (!this.Repository.PersonExists(personName))
            {
                throw new InvalidUserInputException("Person with such name doesn't exists");
            }

            int taskId = ParseIntParameter(this.CommandParameters[1], "id");

            return AssignedTaskToMember(personName, taskId);
        }

        private string AssignedTaskToMember(string personName, int taskId)
        {
            this.Repository.AssignTaskToPerson(personName, taskId);

            return $"Task with Id:{taskId} is assigned to {personName}.";
        }
    }
}
