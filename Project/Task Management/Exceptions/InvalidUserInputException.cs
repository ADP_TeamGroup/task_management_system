﻿using System;

namespace Task_Management.Exceptions
{
    public class InvalidUserInputException : ApplicationException
    {
        public InvalidUserInputException(string massage) : base(massage)
        { }
    }
}
