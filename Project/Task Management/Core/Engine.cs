﻿using System;
using Task_Management.Core.Contracts;
using Task_Management.Commands.Contracts;

namespace Task_Management.Core
{
    public class Engine : IEngine
    {

        private readonly ICommandFactory commandFactory;    

        public Engine(ICommandFactory commandFactory)
        {
            this.commandFactory = commandFactory;
        }

        public void Start()
        {
            while (true)
            {
                try
                {
                    string input = Console.ReadLine().Trim();

                    if (input == string.Empty)
                    {
                        Console.WriteLine(Constants.EmptyCommandError);
                        continue;
                    }

                    if (input.Equals(Constants.StopCommand, StringComparison.InvariantCultureIgnoreCase))
                    {
                        break;
                    }

                    ICommand command = this.commandFactory.Create(input);
                    string result = command.Execute();

                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine(result.Trim());
                    Console.WriteLine(Constants.Separator);
                    Console.ResetColor();
                }
                catch (Exception exception)
                {
                    if (!string.IsNullOrEmpty(exception.Message))
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine(exception.Message);
                        Console.WriteLine(Constants.Separator);
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine(exception);
                        Console.WriteLine(Constants.Separator);
                        Console.ResetColor();
                    }
                }
            }
        }
    }
}
