﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class CreateBug_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
        }
        [TestMethod]
        public void CreateBugCommand_Should_Throw_When_NumberOfParametersAreNotValid()
        {
            //Arrange
            //string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce
            string title = "\"Fix That bug mate\"";
            string description = "\"This bug happens when user tries to log in\"";
            PriorityType priorityType = PriorityType.High;
            SeverityType severityType = SeverityType.Major;

            //Act
            ICommand createBug = commandFactory.Create($"Createbug Teams Board {title} {description} {priorityType} {severityType} ");

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBug.Execute());
        }
        [TestMethod]
        public void CreateBugCommand_Should_Throw_When_TeamDoesnotExist()
        {
            //Arrange
            //string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce
            string title = "\"Fix That bug mate\"";
            string description = "\"This bug happens when user tries to log in\"";
            PriorityType priorityType = PriorityType.High;
            SeverityType severityType = SeverityType.Major;
            string stepsToReproduce = "{{Open Vs}} {{Go to the console}} {{Try to login}}";

            //Act
            ICommand createBug = commandFactory.Create($"Createbug kotaracite Board {title} {description} {priorityType} {severityType} {stepsToReproduce}");

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBug.Execute());
        }
        [TestMethod]
        public void CreateBugCommand_Should_Throw_When_BoarddoesnotExist()
        {
            //Arrange
            //string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce
            string title = "\"Fix That bug mate\"";
            string description = "\"This bug happens when user tries to log in\"";
            PriorityType priorityType = PriorityType.High;
            SeverityType severityType = SeverityType.Major;
            string stepsToReproduce = "{{Open Vs}} {{Go to the console}} {{Try to login}}";

            //Act
            ICommand createBug = commandFactory.Create($"Createbug Teams Board2 {title} {description} {priorityType} {severityType} {stepsToReproduce}");

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBug.Execute());
        }
        [TestMethod]
        public void CreateBugCommand_Should_Throw_When_PriorityIsNotValid()
        {
            //Arrange
            //string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce
            string title = "\"Fix That bug mate\"";
            string description = "\"This bug happens when user tries to log in\"";
            SeverityType severityType = SeverityType.Major;
            string stepsToReproduce = "{{Open Vs}} {{Go to the console}} {{Try to login}}";

            //Act
            ICommand createBug = commandFactory.Create($"Createbug Teams Board {title} {description} major {severityType} {stepsToReproduce}");

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBug.Execute());
        }
        [TestMethod]
        public void CreateBugCommand_Should_BeValid_When_PassedCorrectData()
        {
            //Arrange
            //string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce
            string title = "\"Fix That bug mate\"";
            string description = "\"This bug happens when user tries to log in\"";
            PriorityType priorityType = PriorityType.High;
            SeverityType severityType = SeverityType.Major;
            string stepsToReproduce = "{{Open Vs}} {{Go to the console}} {{Try to login}}";

            //Act
            ICommand createBug = commandFactory.Create($"Createbug Teams Board {title} {description} {priorityType} {severityType} {stepsToReproduce}");

            //Assert
            Assert.AreEqual("Bug with Id:1 was created in board:Board in team:Teams!",createBug.Execute());
        }
    }
}
