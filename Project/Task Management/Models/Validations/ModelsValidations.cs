﻿using Task_Management.Exceptions;

namespace Task_Management.Models.Validations
{
    public static class ModelsValidations
    {
        public static void ValidateStringLength(int value, int minLength, int maxLength, string className, string type)
        {
            if (value < minLength || value > maxLength)
            {
                throw new OutOfRangeException(string.Format(Constants.LengthErrorMessage, className, type, minLength, maxLength));
            }
        }
        public static void CheckIfValueIsNullOrWhiteSpace(string value, string description,string error)
        {
            string errorMassage = string.Format(error, description);

            if (string.IsNullOrWhiteSpace(value)) { throw new InvalidUserInputException(errorMassage); }
        }

        public static void ValidateNonNegative (int value, string field)
        {
            if (value < 0)
            {
                throw new OutOfRangeException(string.Format(Constants.NegativeErrorMessage, field));
            }
        }

        public static void ValidateInRange (int value, int minValue, int maxValue, string field)
        {
            if (value < minValue || value > maxValue && field != "tasks")
            {
                throw new OutOfRangeException(string.Format(Constants.OutOfRangeErrorMessage, field, minValue, maxValue));
            }
        }
    }
}
