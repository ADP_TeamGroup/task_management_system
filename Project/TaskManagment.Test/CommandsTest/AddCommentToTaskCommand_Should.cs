﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class AddCommentToTaskCommand_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        ICommand createBug;
        ICommand createPerson;
        ICommand assignTask;
        
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
            createBug = commandFactory.Create("createbug Teams Board \"Fix That bug mate\" \"This bug happens when user tries to log in\" high major \"Open Vs\" \" Go to the menu\"");
            createBug.Execute();
            createPerson = commandFactory.Create("createPerson Pesho");
            createPerson.Execute();
            assignTask = commandFactory.Create("assigntask Pesho 1");
            assignTask.Execute();
            
        }
        [TestMethod]
        public void AddCommentToTaskCommand_Should_Throw_When_NumberOfAreNotRight()
        {
            //Arrange
            string person = "Pesho";
            int id = 1;
            //Act
            ICommand addComment = commandFactory.Create($"addcomment {person} {id}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => addComment.Execute());
        }
        [TestMethod]
        public void AddCommentToTaskCommand_Should_Throw_When_IdIsNotRight()
        {
            //Arrange
            string person = "Pesho";
            string comment = "{{This task was hard but I did it in two hours}}";
            //Act
            ICommand addComment = commandFactory.Create($"addcomment {person} 2 {comment}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => addComment.Execute());
        }
        [TestMethod]
        public void AddCommentToTaskCommand_Should_Throw_When_PersonDoesNotExist()
        {
            //Arrange
            int id = 1;
            string comment = "{{This task was hard but I did it in two hours}}";
            //Act
            ICommand addComment = commandFactory.Create($"addcomment Gosho {id} {comment}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => addComment.Execute());
        }
        [TestMethod]
        public void AddCommentToTaskCommand_Should_Return_When_ParametersAreValid()
        {
            //Arrange
            string person = "Pesho";
            int id = 1;
            string comment = "{{This task was hard but I did it in two hours}}";
            //Act
            ICommand addComment = commandFactory.Create($"addcomment {person} {id} {comment}");
            //Assert
            Assert.AreEqual("Comment from member:Pesho was added to task with Id:1!",addComment.Execute());
        }
    }
}
