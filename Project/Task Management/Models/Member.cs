﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Exceptions;
using Task_Management.Models.Contracts;
using Task_Management.Models.Validations;

namespace Task_Management.Models
{

    public class Member : IMember
    {
        private string name;
        private readonly IList<IActivityHistory> memberHistory = new List<IActivityHistory>();
        private readonly IList<ITask> tasks = new List<ITask>();

        public Member(string name)
        {
            this.Name = name;

            var memberHistory = new ActivityHistory($"Person with name:{this.Name} was created!");
            this.AddHistory(memberHistory);
        }

        public IList<IActivityHistory> HistoryList { get => new List<IActivityHistory>(this.memberHistory); }

        public string Name
        {
            get { return this.name; }
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Name", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length, Constants.MemberNameMinLength, Constants.MemberNameMaxLength, nameof(Board), "name");
                this.name = value;
            }
        }

        public IList<ITask> Tasks
        {
            get
            {
                return new List<ITask>(tasks);
            }
        }

        public void AddTasksToMember(ITask task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("Task cannot be null");
            }
            else if (this.tasks.Contains(task))
            {
                throw new InvalidUserInputException($"Task with ID:{task.ID} already assigned to {this.Name}!");
            }
            this.tasks.Add(task);
        }

        public void RemoveTaskOfMember(ITask task)
        {
            if (task == null)
            {
                throw new ArgumentNullException("Task cannot be null");
            }
            else if (!this.tasks.Contains(task))
            {
                throw new InvalidUserInputException($"Task with ID:{task.ID} already unassigned from {this.Name}!");
            }

            this.tasks.Remove(task);
        }

        public void AddComment(IComment commentToAdd, ITask taskToAddComment)
        {
            taskToAddComment.AddComment(commentToAdd);
        }

        public void AddHistory(IActivityHistory historyToAdd)
        {
            this.memberHistory.Add(historyToAdd);
        }
    }
}
