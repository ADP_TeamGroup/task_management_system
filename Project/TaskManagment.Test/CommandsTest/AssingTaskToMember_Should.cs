﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class AssingTaskToMember_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        ICommand createBug;
        ICommand createPerson;

        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
            createBug = commandFactory.Create("createbug Teams Board \"Fix That bug mate\" \"This bug happens when user tries to log in\" high major \"Open Vs\" \" Go to the menu\"");
            createBug.Execute();
            createPerson = commandFactory.Create("createPerson Pesho");
            createPerson.Execute();
        }
        [TestMethod]
        public void AssingTaskToMember_Should_Throw_When_AregumentsIsNotRightCount()
        {
            //Arrange
            string name = "Pesho";
            //Act
            ICommand assignPerson = commandFactory.Create($"assigntask {name}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => assignPerson.Execute());
        }
        [TestMethod]
        public void AssingTaskToMember_Should_Throw_When_NameIsInvalid()
        {
            //Arrange
            int id = 1;
            //Act
            ICommand assignPerson = commandFactory.Create($"assigntask Gosho {id}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => assignPerson.Execute());
        }
        [TestMethod]
        public void AssingTaskToMember_Should_Throw_When_IdIsInvalid()
        {
            //Arrange
            string name = "Pesho";
            //Act
            ICommand assignPerson = commandFactory.Create($"assigntask {name} 2");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => assignPerson.Execute());
        }
        [TestMethod]
        public void AssingTaskToMember_Should_Return_When_InputIsValid()
        {
            //Arrange
            string name = "Pesho";
            int id = 1;
            //Act
            ICommand assignPerson = commandFactory.Create($"assigntask {name} {id}");
            //Assert
            Assert.AreEqual("Task with Id:1 is assigned to Pesho.", assignPerson.Execute());
        }
    }
}
