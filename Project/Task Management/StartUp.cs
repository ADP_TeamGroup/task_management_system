﻿using Task_Management.Core;
using Task_Management.Core.Contracts;

namespace Task_Management
{
    class StartUp
    {
        static void Main()
        {
            IRepository repository = new Repository();
            ICommandFactory commandFactory = new CommandFactory(repository);
            IEngine engine = new Core.Engine(commandFactory);
            engine.Start();
        }
    }
}
