﻿using Task_Management.Exceptions;
using System.Collections.Generic;
using Task_Management.Core.Contracts;
using Task_Management.Models;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using System.Linq;

namespace Task_Management.Core
{
    public class Repository : IRepository
    {
        private int nextID;
        private readonly IList<ITeam> teams = new List<ITeam>();
        private readonly IList<IMember> persons = new List<IMember>();
        private readonly IList<ITask> tasks = new List<ITask>();
        private readonly IList<IMember> membersInTeam = new List<IMember>();

        public Repository()
        {
            this.nextID = 0;
        }

        public IList<ITeam> Teams
        {
            get
            {
                return new List<ITeam>(this.teams);
            }
        }

        public IList<IMember> Persons
        {
            get
            {
                return new List<IMember>(this.persons);
            }
        }

        public IList<ITask> Tasks
        {
            get
            {
                return new List<ITask>(this.tasks);
            }
        }

        public IList<IMember> MembersInTeam { get => new List<IMember>(this.MembersInTeam); }

        public IBoard CreateBoard(string boardName, string teamName)
        {
            var board = new Board(boardName);
            var teamToFind = FindTeam(teamName);

            teamToFind.AddBoard(board);

            return board;

        }

        public IMember CreatePerson(string personName)
        {
            var person = new Member(personName);
            this.persons.Add(person);
            return person;
        }

        public ITeam CreateTeam(string name)
        {
            var team = new Team(name);
            this.teams.Add(team);

            return team;
        }

        public IBug CreateBug(string teamName, string boardName, string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce)
        {
            var bug = new Bug(++nextID, title, description, priority, severity, listOfStepsToReproduce);
            var board = FindBoard(teamName, boardName);

            board.Tasks.Add(bug);
            this.tasks.Add(bug);

            return bug;
        }

        public IFeedback CreateFeedback(string teamName, string boardName, string title, string description, int rating, FeedbackStatus status)
        {
            var feedback = new Feedback(++nextID, title, description, rating, status);
            var board = FindBoard(teamName, boardName);

            board.Tasks.Add(feedback);
            this.tasks.Add(feedback);


            return feedback;
        }

        public IStory CreateStory(string teamName, string boardName, string title, string description, PriorityType priority, SizeStory size, StoryStatus status)
        {
            var story = new Story(++nextID, title, description, priority, size, status);
            var board = FindBoard(teamName, boardName);

            board.AddTask(story);
            this.tasks.Add(story);


            return story;
        }

        public IComment CreateComment(string commentMsg, string member)
        {
            return new Comment(commentMsg, member);
        }

        public void AddPersonToTeam(string personName, string teamName)
        {
            var foundTeam = FindTeam(teamName);

            foreach (var person in this.persons)
            {
                if (person.Name == personName)
                {
                    foundTeam.AddMembers(person);
                    this.membersInTeam.Add(person);
                    return;
                }
            }

            throw new InvalidUserInputException($"Person with name:{personName} doens't exists");
        }

        public bool PersonExists(string name)
        {
            foreach (var person in this.persons)
            {
                if (person.Name == name)
                {
                    return true;
                }
            }
            return false;
        }

        public bool TeamExists(string teamName)
        {
            foreach (var team in this.teams)
            {
                if (team.Name == teamName)
                {
                    return true;
                }
            }
            return false;
        }

        public bool BoardExists(string boardName, string teamName)
        {
            if (TeamExists(teamName))
            {
                var team = FindTeam(teamName);

                foreach (var board in team.Boards)
                {
                    if (board.Name == boardName)
                    {
                        return true;
                    }
                }
            }
            else
            {
                throw new InvalidUserInputException($"Team with {teamName} doesn't exist!");
            }

            return false;
        }

        public bool TaskExist(int id)
        {
            foreach (var task in this.tasks)
            {
                if (task.ID == id)
                {
                    return true;
                }
            }
            return false;
        }

        public bool MemberExistInTeam(string personName,string teamName)
        {
            var team = FindTeam(teamName);

            foreach (var member in team.Members)
            {
                if (member.Name == personName)
                {
                    return true;
                }
            }

            return false;
        }

        public ITeam FindTeam(string teamName)
        {
            foreach (var team in this.teams)
            {
                if (team.Name == teamName)
                {
                    return team;
                }
            }

            throw new InvalidUserInputException($"Team with name: {teamName} doesn't exist!");
        }

        public IMember FindPerson(string personName)
        {
            foreach (var person in this.persons)
            {
                if (person.Name == personName)
                {
                    return person;
                }
            }

            throw new InvalidUserInputException($"Person with name: {personName} doesn't exist!");
        }

        private IBoard FindBoard(string teamName, string boardName)
        {
            var team = FindTeam(teamName);

            foreach (var board in team.Boards)
            {
                if (board.Name == boardName)
                {
                    return board;
                }
            }

            throw new InvalidUserInputException($"Board with name: {boardName} doesn't exist!");
        }

        public ITask FindTask(int id)
        {
            foreach (var task in this.tasks)
            {
                if (task.ID == id)
                {
                    return task;
                }
            }
            throw new InvalidUserInputException($"Task with ID:{id} doesn't exist");
        }

        public void AssignTaskToPerson(string personName, int taskId)
        {
            var member = FindPerson(personName);

            foreach (var task in this.tasks)
            {
                if (task.ID == taskId)
                {
                    member.AddTasksToMember(task);
                    return;
                }
            }

            throw new InvalidUserInputException($"Task with Id:{taskId} doesn't exist!");
        }

        public void UnassignTaskOfPerson(string personName, int taskId)
        {
            var member = FindPerson(personName);

            foreach (var task in this.tasks)
            {
                if (task.ID == taskId)
                {
                    member.RemoveTaskOfMember(task);
                    return;
                }
            }

            throw new InvalidUserInputException($"Task with Id:{taskId} doesn't exist!");
        }
    }
}
