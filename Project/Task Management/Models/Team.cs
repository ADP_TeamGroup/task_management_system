﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Validations;

namespace Task_Management.Models
{
    public class Team : ITeam
    {
        private string name;
        private readonly IList<IMember> members = new List<IMember>();
        private readonly IList<IBoard> memberBoards = new List<IBoard>();
        private readonly IList<IActivityHistory> teamHistory = new List<IActivityHistory>();

        public Team(string name)
        {
            this.Name = name;

            var teamHistory = new ActivityHistory($"Team with name:{this.Name} was created!");
            this.AddHistory(teamHistory);
        }
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Name", Constants.ErrorMassageForNullOrWhiteSpace);
                ModelsValidations.ValidateStringLength(value.Length, Constants.TeamNameMinLength, Constants.TeamNameMaxLength, nameof(Team), "Name");
                this.name = value;
            }
        }

        public IList<IMember> Members { get { return new List<IMember>(this.members); } }

        public IList<IBoard> Boards { get { return new List<IBoard>(this.memberBoards); } }

        public IList<IActivityHistory> HistoryList { get => new List<IActivityHistory>(this.teamHistory); }

        public void AddBoard(IBoard board)
        {
            if (board == null)
            {
                throw new ArgumentNullException("board cannot be null");
            }
            this.memberBoards.Add(board);
        }

        public void AddMembers(IMember member)
        {
            if (member == null)
            {
                throw new ArgumentNullException("member cannot be null");
            }
            this.members.Add(member);
        }

        public void AddHistory(IActivityHistory historyToAdd)
        {
            this.teamHistory.Add(historyToAdd);
        }
    }
}
