﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;


namespace TaskManagment.Test.Models.Test.TeamTests
{
    [TestClass]
    public class TeamName_Should
    {
        [TestMethod]
        public void TeamName_Should_Throw_When_ShorterThanMinLength()
        {
            //Arrang
            string name = new string('a',Constants.TeamNameMinLength - 1);
            //Act & Assert
            Assert.ThrowsException<OutOfRangeException>(()=> new Team(name));

        }
        [TestMethod]
        public void TeamName_Should_Throw_When_LongerThanMaxLength()
        {
            //Arrang
            string name = new string('a', Constants.TeamNameMaxLength + 1);
            //Act & Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Team(name));
        }
        [TestMethod]
        public void TeamName_Should_Throw_When_Null()
        {
            //Arrang
            string name = null;
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Team(name));
        }
        [TestMethod]
        public void TeamName_Should_Throw_When_WhiteSpaces()
        {
            //Arrang
            string name = new string(' ',10);
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Team(name));
        }
        [TestMethod]
        public void TeamName_Should_ReturnCorrectName_When_CorrectInput()
        {
            //Arrang
            string name = new string('a', 10);
            //Act
            ITeam  team = new Team(name);
            //Assert
            Assert.AreEqual(name, team.Name);
        }

    }
}
