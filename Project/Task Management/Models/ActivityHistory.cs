﻿
namespace Task_Management.Models
{
    public class ActivityHistory : IActivityHistory
    {
        public ActivityHistory(string history)
        {
            this.History = history;
        }
        public string History { get; protected set; }
    }
}
