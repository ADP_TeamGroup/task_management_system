﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class CreatePersonCommandTest_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam =commandFactory.Create("createperson Person");
            createTeam.Execute();
            
        }
        [TestMethod]
        public void CreatePersonCommandTest_Should_Throw_When_NumberOfArgumentsAreNotCorrect()
        {
            //Arrange
            ICommand createPerson = commandFactory.Create("CreatePerson");
            //Act

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createPerson.Execute());
        }

        [TestMethod]
        public void CreatePersonCommandTest_Should_Throw_When_PeronAlreadyExist()
        {
            //Arrange
            ICommand createPerson = commandFactory.Create("CreatePerson Pesho");
            createPerson.Execute();
            //Act
            ICommand createPerson2 = commandFactory.Create("CreatePerson Pesho");

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createPerson2.Execute());
        }
        [TestMethod]
        public void CreatePersonCommandTest_Should_BeValid_When_CorrectInput()
        {
            //Arrange
            ICommand createPerson = commandFactory.Create("CreatePerson Pesho");
            
            
            //Act

            //Assert
            Assert.AreEqual("Person with name:Pesho was created!", createPerson.Execute());
        }

    }
}
