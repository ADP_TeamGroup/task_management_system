﻿using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;

namespace Task_Management.Models
{
    public abstract class Task : ITask
    {
        private readonly IList<IComment> taskComments = new List<IComment>();
        private readonly IList<IActivityHistory> taskHistory = new List<IActivityHistory>();


        protected Task(int id, string title,string description)
        {
            this.Title = title;
            this.Description = description;
            this.ID = id;
        }

        public int ID { get; }

        public virtual string Title { get; protected set; }

        public virtual string Description { get; protected set; }

        public IList<IActivityHistory> HistoryList { get => new List<IActivityHistory>(this.taskHistory); }

        public IList<IComment> Comments { get => new List<IComment>(this.taskComments); }

        public void AddComment(IComment comment)
        {
            this.taskComments.Add(comment);
        }

        public void AddHistory(IActivityHistory historyToAdd)
        {
            this.taskHistory.Add(historyToAdd);
        }

        public string PrintComments()
        {

            //ToDo
            return "";
        }

        public override string ToString()
        {
            StringBuilder taskInfo = new StringBuilder();
            taskInfo.AppendLine();
            return taskInfo.ToString();
        }
    }
}
