﻿using System.Collections.Generic;

namespace Task_Management.Models.Contracts
{
    public interface ICommentable
    {
        public IList<IComment> Comments { get; }

        public void AddComment(IComment comment);

    }
}
