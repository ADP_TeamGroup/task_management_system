﻿using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using System.Text;
using System.Collections.Generic;

namespace Task_Management.Commands
{
    public class ShowAllBoardsInTeamCommand : BaseCommand
    {
        public ShowAllBoardsInTeamCommand(List<string> parameters, IRepository repository)
                : base(parameters, repository) { }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, 1, this.CommandParameters.Count);

                throw new InvalidUserInputException(errorMsg);
            }

            string teamName = this.CommandParameters[0];

            return this.ShowAllBoardsInTeam(teamName);
        }

        private string ShowAllBoardsInTeam(string teamName)
        {
            var team = this.Repository.FindTeam(teamName);

            if (team.Boards.Count > 0)
            {
                var sbBoards = new StringBuilder();

                foreach (var board in team.Boards)
                {
                    sbBoards.AppendLine(board.Name);
                }

                return sbBoards.ToString().Trim();
            }

            throw new OutOfRangeException($"Team with name:{teamName} doesn't have any boards!");
        }
    }
}
