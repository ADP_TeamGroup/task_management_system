﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;

namespace TaskManagment.Test.Models.Test.BugTest
{
    [TestClass]
    public class BugTitle_Should
    {
        //string title, string description, PriorityType priority, SeverityType severity, BugStatus status
        //Bug class constants
        //public const int BugTitleMinLength = 10;
        //public const int BugTitleMaxLength = 50;
        //public const int BugDescrMinLength = 10;
        //public const int BugDescrMaxLength = 500;
        PriorityType priority = PriorityType.Medium;
        SeverityType severity = SeverityType.Major;
        string step = "Open VS";
        int id = 5;

        [TestMethod]
        public void BugTitle_Should_Throw_When_LengthTooShort()
        {
            //Arrange
            string title = new string('a', 9);
            string description = "Can not load in the matrix";
            //Act
            //Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Bug(id, title, description, priority, severity, step));
        }

        [TestMethod]
        public void BugTitle_Should_Throw_When_LengthTooLong()
        {
            //Arrange
            string title = new string('a', 51);
            string description = "Can not load in the matrix";

            //Act
            //Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Bug(id, title, description, priority, severity, step));
        }
        [TestMethod]
        public void BugTitle_Should_Throw_When_Null()
        {
            //Arrange
            string title = null;
            string description = "Can not load in the matrix";

            //Act
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Bug(id,title, description, priority, severity, step));
        }
        [TestMethod]
        public void BugTitle_Should_Throw_When_Whitespaces()
        {
            //Arrange
            string title = new string(' ', 14);
            string description = "Can not load in the matrix";

            //Act
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Bug(id,title, description, priority, severity, step));
        }
        [TestMethod]
        public void BugTitle_Should_BeCorrect_When_CorrectInput()
        {
            //Arrange
            string title = "Fix that bug mate";
            string description = "Can not load in the matrix";

            //Act
            ITask bug = new Bug(id,title, description, priority, severity, step);
            //Assert
            Assert.AreEqual(title, bug.Title);
        }
    }
}

