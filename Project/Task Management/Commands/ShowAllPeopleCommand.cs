﻿using System.Text;
using Task_Management.Core.Contracts;

namespace Task_Management.Commands
{
    public class ShowAllPeopleCommand : BaseCommand
    {
        public ShowAllPeopleCommand(IRepository repository)
            :base(repository)
        {

        }
        public override string Execute()
        {
            return ShowAllPeople();
        }

        private string ShowAllPeople()
        {
            if (this.Repository.Persons.Count > 0)
            {
                var sbPeople = new StringBuilder();

                foreach (var person in this.Repository.Persons)
                {
                    sbPeople.AppendLine(person.Name.ToString());

                }
                return sbPeople.ToString().Trim();
            }
            else
            {
                return "There are no people!";
            }
        }
    }
}
