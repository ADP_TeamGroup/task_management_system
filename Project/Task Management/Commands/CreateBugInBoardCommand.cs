﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models.Enums;

namespace Task_Management.Commands
{
    public class CreateBugInBoardCommand : BaseCommand
    {
        public CreateBugInBoardCommand(List<string> parameters, IRepository repository)
            : base(parameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count < Constants.BugMinCountParameters)
            {
                string errorMsg = string.Format(Constants.BugNumberOFArgumentsErrorMsg, Constants.BugMinCountParameters, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMsg);
            }

            string teamName = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];

            if (!this.Repository.BoardExists(boardName, teamName))
            {
                throw new InvalidUserInputException($"Board with name:{boardName} in team:{teamName} already exists!");
            }


            string title = this.CommandParameters[2];
            string description = this.CommandParameters[3];
            PriorityType priority = this.ParseEnumParameter<PriorityType>(this.CommandParameters[4], "priority");
            SeverityType severity = this.ParseEnumParameter<SeverityType>(this.CommandParameters[5], "severity");

            List<string> steps = new List<string>();

            for (int i = 6; i < CommandParameters.Count; i++)
            {
                steps.Add(CommandParameters[i]);
            }

            return CreateBug(teamName, boardName, title, description
                        , priority, severity
                        , steps.ToArray());
        }

        private string CreateBug(string teamName, string boardName, string title, string description
                        , PriorityType priority, SeverityType severity
                        , params string[] listOfStepsToReproduce)
        {
            var bug = this.Repository.CreateBug(teamName, boardName, title, description
                        , priority, severity
                        , listOfStepsToReproduce);

            return $"Bug with Id:{bug.ID} was created in board:{boardName} in team:{teamName}!";
        }

    }
}
