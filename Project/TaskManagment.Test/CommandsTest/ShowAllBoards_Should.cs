﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class ShowAllBoards_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        ICommand createBoard2;

        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
            createBoard2 = commandFactory.Create("createBoard Teams Board2");
            createBoard2.Execute();

        }

        [TestMethod]
        public void ShowAllBoards_Should_Throw_When_ParametersAreNotRightAmount()
        {
            //Arrange
            //Act
            ICommand showAllBoards = commandFactory.Create("showallteamboards");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => showAllBoards.Execute());
        }
        [TestMethod]
        public void ShowAllBoards_Should_Throw_When_TeamNameIsNotValid()
        {
            //Arrange
            //Act
            ICommand showAllBoards = commandFactory.Create("showallteamboards error" );
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => showAllBoards.Execute());
        }
        [TestMethod]
        public void ShowAllBoards_Should_Return_When_ArgumentsAreValid()
        {
            //Arrange
            string teamName = "Teams";
            //Act
            ICommand showAllBoards = commandFactory.Create($"showallteamboards {teamName}");
            var sbBoards = new StringBuilder();
            sbBoards.AppendLine("Board");
            sbBoards.AppendLine("Board2");
            //Assert
            Assert.AreEqual(sbBoards.ToString().Trim(), showAllBoards.Execute());
        }
    }
}
