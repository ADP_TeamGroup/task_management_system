﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models.Validations;

namespace Task_Management.Models
{
    class Feedback : Task, IFeedback
    {
        private int rating;

        public Feedback(int id, string title, string description, int rating, FeedbackStatus status)
            : base(id, title, description)
        {
            this.Rating = rating;
            this.Status = status;

            var feedbackHistory = new ActivityHistory($"Feedback with Id:{this.ID} was created!");
            this.AddHistory(feedbackHistory);
        }

        public override string Title
        {
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Title", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length,
                                                       Constants.FeedbackTitleMinLength,
                                                       Constants.FeedbackTitleMaxLength,
                                                       this.GetType().Name,
                                                       "title");
                base.Title = value;
            }
        }

        public override string Description
        {
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Description", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length,
                                                       Constants.FeedbackDescrMinLength,
                                                       Constants.FeedbackDescrMaxLength,
                                                       this.GetType().Name,
                                                       "description");
                base.Description = value;
            }
        }

        public int Rating
        {
            get
            {
                return this.rating;
            }
            set
            {
                ModelsValidations.ValidateNonNegative(value, nameof(Rating));

                ModelsValidations.ValidateInRange(value, Constants.FeedbackRatingMinValue, Constants.FeedbackRatingMaxValue, nameof(Rating));
                this.rating = value;
            }
        }

        public FeedbackStatus Status { get; set; }
    }
}
