﻿using System.Collections.Generic;

namespace Task_Management.Models.Contracts
{
    public interface IHistory
    {
        public IList<IActivityHistory> HistoryList { get; }

        public void AddHistory(IActivityHistory historyToAdd);

    }
}
