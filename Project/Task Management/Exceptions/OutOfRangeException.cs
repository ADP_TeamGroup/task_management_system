﻿using System;

namespace Task_Management.Exceptions
{
    public class OutOfRangeException : ApplicationException
    {
        public OutOfRangeException(string message) : base(message)
        { }

    }
}
