﻿

namespace Task_Management.Models.Contracts
{
    public interface IHasID
    {
        public int ID { get; }

    }
}
