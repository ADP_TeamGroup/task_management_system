﻿using System.Collections.Generic;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class UnnasignTaskOfMember : BaseCommand
    {
        public UnnasignTaskOfMember(List<string> parameters, IRepository repository)
            : base(parameters, repository) { }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 2)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, 2, this.CommandParameters.Count);

                throw new InvalidUserInputException(errorMsg);
            }

            string personName = this.CommandParameters[0];

            if (!this.Repository.PersonExists(personName))
            {
                throw new InvalidUserInputException("Person with such name doesn't exists");
            }

            int taskId = ParseIntParameter(this.CommandParameters[1], "id");

            return UnassignTeamOfTask(personName, taskId);
        }

        private string UnassignTeamOfTask(string memberName, int taskId)
        {
            this.Repository.UnassignTaskOfPerson(memberName, taskId);

            return $"Task with Id:{taskId} was unnasigned from {memberName}.";
        }
    }
}
