﻿
namespace Task_Management.Models.Contracts
{
    public interface ITask : ICommentable, IHasID, IHistory
    {
        public string Title { get; }

        public string Description { get; }


        public string PrintComments();
    }
}
