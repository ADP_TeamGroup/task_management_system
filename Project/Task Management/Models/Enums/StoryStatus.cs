﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_Management.Models.Enums
{
    public enum StoryStatus
    {
        NotDone, 
        InProgress,
        Done
    }
}
