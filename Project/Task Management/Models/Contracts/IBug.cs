﻿using System.Collections.Generic;
using Task_Management.Models.Enums;

namespace Task_Management.Models.Contracts
{
    public interface IBug : ITask
    {
        public IList<string> Steps { get; }

        public PriorityType Priority { get; set; }

        public SeverityType Severity { get; set; }

        public BugStatus Status { get; set; }
        public IMember Assignee { get; }


    }
}
