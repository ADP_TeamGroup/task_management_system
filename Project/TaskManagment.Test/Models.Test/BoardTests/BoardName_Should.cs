﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Exceptions;
using Task_Management.Models;

namespace TaskManagment.Test.Models.Test.BoardTests
{
    [TestClass]
    public class BoardName_Should
    {
        [TestMethod]
        public void BoardName_Should_Throw_When_NameLengthShorterThanMinLength()
        {
            //Arrange
            string name = new string('a', Constants.BoardNameMinLength - 1);
            //Act & Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Board(name));
        }
        [TestMethod]
        public void BoardName_Should_Throw_When_NameLengthLongerThanMaxLength()
        {
            //Arrange
            string name = new string('a', Constants.MemberNameMaxLength + 1);
            //Act & Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Board(name));
        }
        [TestMethod]
        public void BoardName_Should_Throw_When_NameIsNull()
        {
            //Arrange
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Board(null));
        }
        [TestMethod]
        public void BoardName_Should_Throw_When_NameIsWhitespace()
        {
            //Arrange
            string name = new string(' ', 6);
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Board(name));
        }
        [TestMethod]
        public void BoardrName_Should_ReturnValidName_When_NameIsCorrect()
        {
            //Arrange
            string name = new string('a', 6);
            Board board = new Board(name);
            //Act & Assert
            Assert.AreEqual(name, board.Name);
        }
    }
}
