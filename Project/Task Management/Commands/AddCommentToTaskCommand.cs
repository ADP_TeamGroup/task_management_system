﻿using System.Collections.Generic;
using System.Linq;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models.Contracts;
using Task_Management.Models.Validations;

namespace Task_Management.Commands
{
    public class AddCommentToTaskCommand : BaseCommand
    {
        public AddCommentToTaskCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository) { }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 3)
            {
                string errorMassage = string.Format(Constants.NumberOFArgumentsErrorMsg, 3, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMassage);
            }

            string member = this.CommandParameters[0];
            int taskId = ParseIntParameter(this.CommandParameters[1], "taskId");

            string commentMsg = this.CommandParameters[2];

            return AddCommentToTask(member, taskId, commentMsg);
        }

        private string AddCommentToTask(string member, int taskId, string commentMsg)
        {
            var person = this.Repository.FindPerson(member);
            ITask task;
            if (person.Tasks.Any(task => task.ID == taskId))
            {
                task = (ITask)person.Tasks.FirstOrDefault(task => task.ID == taskId);
            }
            else
            {
                throw new InvalidUserInputException($"Task with id: {taskId} doesn't exist");
            }
            ModelsValidations.ValidateInRange(taskId, 0, person.Tasks.Count, "tasks");

            var comment = this.Repository.CreateComment(commentMsg, member);

            person.AddComment(comment, task);

            return $"Comment from member:{member} was added to task with Id:{taskId}!";
        }
    }
}
