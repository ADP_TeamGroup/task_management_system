﻿using System.Collections.Generic;

namespace Task_Management.Models.Contracts
{
    public interface ITeam : IHistory
    {
        public string Name { get; }

        public IList<IMember> Members { get; }

        public IList<IBoard> Boards { get; }

        public void AddMembers(IMember member);
        public void AddBoard(IBoard board);

    }
}
