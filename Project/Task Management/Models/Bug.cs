﻿using System.Collections.Generic;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models.Validations;

namespace Task_Management.Models
{
    public class Bug : Task, IBug
    {
        private readonly IList<string> steps = new List<string>();

        public Bug(int id, string title, string description, PriorityType priority, SeverityType severity, params string[] listOfStepsToReproduce)
            : base(id, title, description)
        {

            this.Status = BugStatus.Active; //Starting status when assigned need to be active
            this.Priority = priority;
            this.Severity = severity;

            AddSteps(listOfStepsToReproduce);

            var bugHistory = new ActivityHistory($"Bug with Id:{this.ID} was created!");
            this.AddHistory(bugHistory);

        }

        public override string Title
        {
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Title", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length,
                                                       Constants.BugTitleMinLength,
                                                       Constants.BugTitleMaxLength,
                                                       this.GetType().Name,
                                                       "title");
                base.Title = value;
            }
        }

        public override string Description
        {
            protected set
            {

                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Description", Constants.ErrorMassageForNullOrWhiteSpace);


                ModelsValidations.ValidateStringLength(value.Length,
                                                       Constants.BugDescrMinLength,
                                                       Constants.BugDescrMaxLength,
                                                       this.GetType().Name,
                                                       "description");
                base.Description = value;
            }
        }

        public IList<string> Steps { get => new List<string>(this.steps); }

        public PriorityType Priority { get; set; }

        public SeverityType Severity { get; set; }

        public BugStatus Status { get; set; }

        public IMember Assignee { get; }

        public void AddSteps(string[] stepsToReproduce)
        {
            foreach (var step in stepsToReproduce)
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(step, "Step", Constants.ErrorMassageForNullOrWhiteSpace);
                this.steps.Add(step);
            }
        }
    }
}
