﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;


namespace TaskManagment.Test.Models.Test.MemberTest
{
    [TestClass]
    public class MemberName_Should
    {
        [TestMethod]
        public void MemberName_Should_Throw_When_NameLengthShorterThanMinLength()
        {
            //Arrange
            string name = new string('a', Constants.MemberNameMinLength - 1);
            //Act & Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Member(name));
        }
        [TestMethod]
        public void MemberName_Should_Throw_When_NameLengthLongerThanMaxLength()
        {
            //Arrange
            string name = new string('a', Constants.MemberNameMaxLength + 1);
            //Act & Assert
            Assert.ThrowsException<OutOfRangeException>(() => new Member(name));
        }
        [TestMethod]
        public void MemberName_Should_Throw_When_NameIsNull()
        {
            //Arrange
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Member(null));
        }
        [TestMethod]
        public void MemberName_Should_Throw_When_NameIsWhitespace()
        {
            //Arrange
            string name = new string(' ', 6);
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => new Member(name));
        }
        [TestMethod]
        public void MemberName_Should_ReturnValidName_When_NameIsCorrect()
        {
            //Arrange
            string name = new string('a', 6);
            Member member = new Member(name);
            //Act & Assert
            Assert.AreEqual(name, member.Name);
        }
    }
}
