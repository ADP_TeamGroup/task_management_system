﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands.Contracts;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public abstract class BaseCommand : ICommand
    {
        protected BaseCommand(IRepository repository)
        {
            this.Repository = repository;
        }
        protected BaseCommand(IList<string> commandParameters, IRepository repository)
        {
            this.CommandParameters = commandParameters;
            this.Repository = repository;
        }

        protected IList<string> CommandParameters { get; }
        protected IRepository Repository { get; }

        public abstract string Execute();

        protected TEnum ParseEnumParameter<TEnum>(string strEnumValue, string parameterName) where TEnum : struct
        {
            if (Enum.TryParse(strEnumValue, true, out TEnum result))
            {
                return result;
            }
            throw new InvalidUserInputException($"Invalid value for {parameterName}.");
        }

        protected int ParseIntParameter(string value, string parameterName)
        {
            if (int.TryParse(value, out int result))
            {
                return result;
            }
            throw new InvalidUserInputException($"Invalid value for {parameterName}. Should be an integer number.");
        }
    }
}
