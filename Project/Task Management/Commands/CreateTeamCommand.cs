﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class CreateTeamCommand : BaseCommand
    {
        public CreateTeamCommand(List<string> parameters, IRepository repository)
            : base(parameters, repository)
        { }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                string errorMessage = string.Format(Constants.NumberOFArgumentsErrorMsg, 1, this.CommandParameters.Count);

                throw new InvalidUserInputException(errorMessage);
            }

            string teamName = this.CommandParameters[0];

            if (this.Repository.TeamExists(teamName))
            {
                throw new InvalidUserInputException($"Team with name:{teamName} already exists!");
            }


            return CreateTeam(teamName);
        }

        private string CreateTeam(string teamName)
        {
            this.Repository.CreateTeam(teamName);

            return $"Team with name:{teamName} was created!";
        }
    }
}
