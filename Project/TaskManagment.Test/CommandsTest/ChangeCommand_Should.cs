﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class ChangeCommand_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        ICommand createBug;


        ICommand createStory;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
            createBug = commandFactory.Create("createbug Teams Board \"Fix That bug mate\" \"This bug happens when user tries to log in\" low major \"Open Vs\" \" Go to the menu\"");
            createBug.Execute();
            createStory = commandFactory.Create($"Createstory Teams Board \"There is ne feature u need to implement\" \"This new feature is new board function that the client will be able to play tetris when he waits to log in :D\" low small inprogress");
            createStory.Execute();
        }
        [TestMethod]
        public void ChangeCommand_Should_Throw_NumberOfParametersIsNotCorrect()
        {
            //Arrange
            int id = 1;
            //Act
            ICommand changeCommand = commandFactory.Create($"change {id}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => changeCommand.Execute());
        }
        [TestMethod]
        public void ChangeCommand_Should_Throw_IdIsNotCorrect()
        {
            //Arrange
            string type = "priority";
            string changeTo = "high";
            //Act
            ICommand changeCommand = commandFactory.Create($"change 4 {type} {changeTo}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => changeCommand.Execute());
        }
        [TestMethod]
        public void ChangeCommand_Should_Throw_TypeIsNotCorrect()
        {
            //Arrange
            int id = 1;
            string changeTo = "high";
            //Act
            ICommand changeCommand = commandFactory.Create($"change {id} error {changeTo}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => changeCommand.Execute());
        }
        [TestMethod]
        public void ChangeCommand_Should_Throw_ChangedToIsNotCorrect()
        {
            //Arrange
            int id = 1;
            string type = "priority";
            //Act
            ICommand changeCommand = commandFactory.Create($"change {id} {type} error");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => changeCommand.Execute());
        }
        [TestMethod]
        public void ChangeCommand_Should_Return_When_InputIsValid()
        {
            //Arrange
            int id = 1;
            string type = "priority";
            string changeTo = "high";
            //Act
            ICommand changeCommand = commandFactory.Create($"change {id} {type} {changeTo}");
            //Assert
            Assert.AreEqual("priority of task with ID:1 is changed from:Low to :high.", changeCommand.Execute());
        }
        [TestMethod]
        public void ChangeCommand_Should_Return_When_InputForStoryIsValid()
        {
            //Arrange
            int id = 2;
            string type = "priority";
            string changeTo = "high";
            //Act
            ICommand changeCommand = commandFactory.Create($"change {id} {type} {changeTo}");
            //Assert
            Assert.AreEqual("priority of task with ID:2 is changed from:Low to :high.", changeCommand.Execute());
        }
    }
}
