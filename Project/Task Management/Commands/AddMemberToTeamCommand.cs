﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class AddMemberToTeamCommand : BaseCommand
    {
        public AddMemberToTeamCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {

        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 2)
            {
                string errorMassage = string.Format(Constants.NumberOFArgumentsErrorMsg, 2, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMassage);
            }

            string personName = CommandParameters[0];
            string teamName = CommandParameters[1];

            if (!this.Repository.PersonExists(personName))
            {
                throw new InvalidUserInputException($"Person with name:{personName} doens't exists");
            }

            if (this.Repository.MemberExistInTeam(personName,teamName))
            {
                throw new InvalidUserInputException($"Member with name:{personName} already exist in team:{teamName}!");
            }

            return AddMember(personName, teamName);

        }

        private string AddMember(string name, string teamName)
        {
            this.Repository.AddPersonToTeam(name, teamName);

            return $"Person with name:{name} was added in team:{teamName}";
        }
    }
}
