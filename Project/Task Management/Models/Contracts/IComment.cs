﻿
namespace Task_Management.Models.Contracts
{
    public interface IComment
    {
        string CommentMsg { get; }

        string Member { get; }
    }
}
