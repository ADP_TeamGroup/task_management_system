﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class CreateBoardCommand_Should
    {

        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
        }
        [TestMethod]
        [DataRow("createboard")]
        [DataRow("createboard Teams")]
        public void CreateBoardCommand_Should_Throw_When_NumberOfArgumentsAreNotCorrect(string input)
        {
            //Arrange
            ICommand createBoard = commandFactory.Create(input);
            //Act

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBoard.Execute());
        }

        [TestMethod]
        
        public void CreateBoardCommand_Should_Throw_When_TeamDoesnotExist()
        {
            //Arrange
            ICommand createBoard = commandFactory.Create("createboard Kotkite Board");
            //Act

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBoard.Execute());
        }
        [TestMethod]

        public void CreateBoardCommand_Should_Throw_When_BoardWithNameAlreadyExists()
        {
            //Arrange
            ICommand createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
            ICommand createBoard1 = commandFactory.Create("createboard Teams Board");

            //Act

            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createBoard1.Execute());
        }
        [TestMethod]
        public void CreateBoardCommand_Should_ReturnValidOutputMassage_When_CorrectInput()
        {
            //Arrange
            ICommand createBoard = commandFactory.Create("createboard Teams Board");
            

            //Act

            //Assert
            Assert.AreEqual("Board with name: Board in team:Teams was created!", createBoard.Execute());
        }


    }
}
