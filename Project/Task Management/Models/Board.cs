﻿using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Validations;

namespace Task_Management.Models
{
    public class Board : IBoard
    {
        private string name;
        private readonly IList<ITask> boardTasks = new List<ITask>();

        public Board(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            protected set
            {
                ModelsValidations.CheckIfValueIsNullOrWhiteSpace(value, "Name", Constants.ErrorMassageForNullOrWhiteSpace);

                ModelsValidations.ValidateStringLength(value.Length, Constants.BoardNameMinLength, Constants.BoardNameMaxLength, nameof(Board), "name");
                this.name = value;
            }
        }

        public IList<ITask> Tasks
        {
            get
            {
                return new List<ITask>(this.boardTasks);
            }
        }

        public void AddTask(ITask task)
        {
            boardTasks.Add(task);
        }
    }
}
