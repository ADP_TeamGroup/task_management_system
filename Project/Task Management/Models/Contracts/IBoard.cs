﻿using System.Collections.Generic;

namespace Task_Management.Models.Contracts
{
    public interface IBoard
    {
        public string Name { get; }

        public IList<ITask> Tasks { get; }

        public void AddTask(ITask task);

        // TODO
        // activity history
    }
}
