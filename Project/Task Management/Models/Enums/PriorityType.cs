﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_Management.Models.Enums
{
   public enum PriorityType
    {
        High,
        Medium,
        Low
    }
}
