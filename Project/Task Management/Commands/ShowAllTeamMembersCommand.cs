﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class ShowAllTeamMembersCommand : BaseCommand
    {
        public ShowAllTeamMembersCommand(List<string> parameters, IRepository repository)
             : base(parameters, repository)
        {

        }
        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, 1, this.CommandParameters.Count);

                throw new InvalidUserInputException(errorMsg);
            }

            string teamName = this.CommandParameters[0];

            return this.ShowAllTeamMembers(teamName);
        }

        private string ShowAllTeamMembers(string teamName)
        {
            var team = this.Repository.FindTeam(teamName);
            if (Repository.Teams.Count > 0)
            {
                var sb = new StringBuilder();

                foreach (var member in team.Members)
                {
                    sb.AppendLine(member.Name.ToString());

                }
                return sb.ToString().Trim();
            }

            return "There are no teams!";

        }
    }
}
