﻿
using System;

public class Constants
{
    //Error massages constants
    public const string LengthErrorMessage = "{0} {1} must be between {2} and {3} symbols long!";
    public const string ErrorMassageForNullOrWhiteSpace = "{0} cannot be null or whitespace";

    public const string NegativeErrorMessage = "{0} must be a positive number";
    public const string OutOfRangeErrorMessage = "{0} must be between {1} and {2}";

    public const string NumberOFArgumentsErrorMsg = "Invalid number of arguments.Expected: {0}, Received: {1}";
    public const string BugNumberOFArgumentsErrorMsg = "Invalid number of arguments.Expected: at least {0}, Received: {1}";
    public const string ListingNumberOfArgumentsErrorMsg = "Invalid number of arguments. Expected: {0} or {1}, Received: {2}";

    public const string TaskDoesntExistErrorMsg = "There aren't any tasks with this Id assigned to this member!";

    //Engine messages and comands
    public const string StopCommand = "Exit";
    public const string EmptyCommandError = "Command can't be empty.";
    public const string Separator = "\n";

    //Bug class constants
    public const int BugTitleMinLength = 10;
    public const int BugTitleMaxLength = 50;
    public const int BugDescrMinLength = 10;
    public const int BugDescrMaxLength = 500;
    public const int BugMinCountParameters = 8;


    //Story class constants
    public const int StoryTitleMinLength = 10;
    public const int StoryTitleMaxLength = 50;
    public const int StoryDescrMinLength = 10;
    public const int StoryDescrMaxLength = 500;
    public const int StoryMinCountParameters = 7;


    //Feedback class constants
    public const int FeedbackTitleMinLength = 10;
    public const int FeedbackTitleMaxLength = 50;
    public const int FeedbackDescrMinLength = 10;
    public const int FeedbackDescrMaxLength = 500;
    public const int FeedbackRatingMinValue = 1;
    public const int FeedbackRatingMaxValue = 10;
    public const int FeedbackMinCountParameters = 6;


    //Team class constants
    public const int TeamNameMinLength = 5;
    public const int TeamNameMaxLength = 15;

    //Member class constants
    public const int MemberNameMinLength = 5;
    public const int MemberNameMaxLength = 15;

    //Board class constants
    public const int BoardNameMinLength = 5;
    public const int BoardNameMaxLength = 10;
}
