﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class CreateBoardCommand : BaseCommand
    {
        public CreateBoardCommand(List<string> parameters, IRepository repository)
            : base(parameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 2)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, 2, this.CommandParameters.Count);

                throw new InvalidUserInputException(errorMsg);
            }

            string teamName = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];

            if (this.Repository.BoardExists(boardName, teamName))
            {
                throw new InvalidUserInputException($"Board with name:{boardName} in team:{teamName} already exists!");
            }

            return CreateBoard(boardName, teamName);
        }

        private string CreateBoard(string boardName, string teamName)
        {
            this.Repository.CreateBoard(boardName, teamName);

            return $"Board with name: {boardName} in team:{teamName} was created!";
        }

    }
}
