﻿using System.Collections.Generic;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models.Enums;

namespace Task_Management.Commands
{
    public class CreateFeedbackInBoardCommand : BaseCommand
    {
        public CreateFeedbackInBoardCommand(List<string> parameters, IRepository repository)
            : base(parameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != Constants.FeedbackMinCountParameters)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, Constants.FeedbackMinCountParameters, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMsg);
            }

            string teamName = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];

            if (!this.Repository.BoardExists(boardName, teamName))
            {
                throw new InvalidUserInputException($"Board with name:{boardName} in team:{teamName} already exists!");
            }


            string title = this.CommandParameters[2];
            string description = this.CommandParameters[3];
            int rating = this.ParseIntParameter(this.CommandParameters[4], "rating");
            FeedbackStatus status = this.ParseEnumParameter<FeedbackStatus>(this.CommandParameters[5], "status");

            return CreateFeedback(teamName, boardName, title, description, rating, status);
        }

        private string CreateFeedback(string teamName, string boardName, string title, string description
                                     , int rating, FeedbackStatus status)
        {
            var feedback = this.Repository.CreateFeedback(teamName, boardName, title, description
                        , rating, status);

            return $"Feedback with Id:{feedback.ID} was created in board:{boardName} in team:{teamName}!";
        }

    }
}
