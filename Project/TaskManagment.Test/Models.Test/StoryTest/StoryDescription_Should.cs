﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;


namespace TaskManagment.Test.Models.Test.StoryTest
{
    [TestClass]
    public class StoryDescription_Should
    {
        const PriorityType priority = PriorityType.Medium;
        const SizeStory size = SizeStory.Large;
        const StoryStatus storyStatus = StoryStatus.InProgress;

        const string title = "\"You can do it mate\"";
        const int id = 1;
        [TestMethod]
        public void StoryDescription_Should_Throw_When_DescriptionIsNull()
        {

            Assert.ThrowsException<InvalidUserInputException>(() => new Story(id, title, null, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryDescription_Should_Throw_When_DescriptionIsWhiteSpace()
        {
            string description = new string(' ', 9);
            Assert.ThrowsException<InvalidUserInputException>(() => new Story(id, title, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryDescription_Should_Throw_When_DescriptionLengthIsTooShort()
        {
            string description = new string('a', Constants.StoryDescrMinLength -1);
            Assert.ThrowsException<OutOfRangeException>(() => new Story(id, title, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryDescription_Should_Throw_When_DescriptionLengthIsTooLong()
        {
            string description = new string('a', Constants.StoryDescrMaxLength + 1);
            Assert.ThrowsException<OutOfRangeException>(() => new Story(id, title, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryDescription_Should_Return_When_InputIsValid()
        {
            string description = new string('a', Constants.StoryDescrMaxLength - 1);
            Story story = new Story(id, title, description, priority, size, storyStatus);
            Assert.AreEqual(description,story.Description);
        }
    }
}
