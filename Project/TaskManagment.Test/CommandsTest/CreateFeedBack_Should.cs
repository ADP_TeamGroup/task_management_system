﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;

namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class CreateFeedBack_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        private const string teamName = "Teams";
        private const string boardName = "Board";
        private const string title = "\"Feedback for ma man Pesho\"";
        private const string description = "\"Pesho, you are  doing great at the workplace\"";
        private const int rating = 10;
        private const FeedbackStatus feedbackStatus = FeedbackStatus.Unscheduled;
        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create($"createteam {teamName}");
            createTeam.Execute();
            createBoard = commandFactory.Create($"createboard {teamName} {boardName}");
            createBoard.Execute();
        }
        [TestMethod]
        public void CreateFeedback_Should_Throw_When_ArgumentsAreNotValid()
        {
            //Arrange
            ICommand createFeedback = commandFactory.Create($"createFeedback {teamName} {boardName} {title} {description} {rating} ");
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createFeedback.Execute());
        }
        [TestMethod]
        public void CreateFeedback_Should_Throw_When_TeamNameIsNotValid()
        {
            //Arrange
            ICommand createFeedback = commandFactory.Create($"createFeedback Kotkite {boardName} {title} {description} {rating} {feedbackStatus}");
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createFeedback.Execute());
        }
        [TestMethod]
        public void CreateFeedback_Should_Throw_When_BoardNameIsNotValid()
        {
            //Arrange
            ICommand createFeedback = commandFactory.Create($"createFeedback {teamName} Boards {title} {description} {rating} {feedbackStatus}");
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createFeedback.Execute());
        }
        [TestMethod]
        public void CreateFeedback_Should_Throw_When_RatingIsNotValid()
        {
            //Arrange
            ICommand createFeedback = commandFactory.Create($"createFeedback {teamName} {boardName} {title} {description} 2.56 {feedbackStatus}");
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createFeedback.Execute());
        }
        [TestMethod]
        public void CreateFeedback_Should_Throw_When_StatusIsNotValid()
        {
            //Arrange
            ICommand createFeedback = commandFactory.Create($"createFeedback {teamName} {boardName} {title} {description} {rating} status");
            //Act & Assert
            Assert.ThrowsException<InvalidUserInputException>(() => createFeedback.Execute());
        }
        [TestMethod]
        public void CreateFeedback_Should_ReturnMassage_When_InputIsValid()
        {
            //Arrange
            ICommand createFeedback = commandFactory.Create($"createFeedback {teamName} {boardName} {title} {description} {rating} {feedbackStatus}");
            //Act & Assert
            Assert.AreEqual("Feedback with Id:1 was created in board:Board in team:Teams!", createFeedback.Execute());
        }
    }
}
