﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;

namespace TaskManagment.Test.Models.Test.MemberTest
{
    [TestClass]
    public class MemberAddTask_Should
    {
        [TestMethod]
        public void MemberAddTask_Should_Add_Correctly_WhenCorrectTaskIsPassed()
        {
            //Arrange
            IMember member = new Member("Gosho");
            ITask task = new Bug(5,"Fix that bug mate", "Can not load in the matrix", PriorityType.Medium, SeverityType.Critical, "Open Vs", "Go to the tools");
            //Act
            member.AddTasksToMember(task);
            Assert.AreEqual(member.Tasks.Count, 1);
        }
        [TestMethod]
        public void MemberAddTask_Should_Throw_When_TaskIsNull()
        {
            //Arrange
            IMember member = new Member("Gosho");
           
            //Assert
            Assert.ThrowsException<ArgumentNullException>(() => member.AddTasksToMember(null));
        }
    }
}
