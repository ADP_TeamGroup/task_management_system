﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;
namespace TaskManagment.Test.Models.Test.TeamTests
{
    [TestClass]
    public class TeamMembers_Should
    {
        [TestMethod]
        public void TeamMembers_Should_ReturnCopy_When_ThereAreMembersAdded()
        {
            //Arrange
            string nameOfTeam = "Gotinite";
            ITeam team = new Team(nameOfTeam);
            IMember member = new Member("Vankata");
            //Act
            team.AddMembers(member);

            //Assert
            Assert.AreEqual(1, team.Members.Count);
            Assert.AreEqual(member, team.Members[0]);
        }
        [TestMethod]
        public void TeamMembers_Should_Throw_When_MemberIsNull()
        {
            //Arrange
            string nameOfTeam = "Gotinite";
            ITeam team = new Team(nameOfTeam);
            //Act
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => team.AddMembers(new Member(null)));
        }
    }
}
