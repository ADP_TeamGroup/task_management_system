﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;

namespace Task_Management.Commands
{
    public class ShowAllTeamsCommand : BaseCommand
    {
        public ShowAllTeamsCommand(IRepository repository)
            : base(repository)
        {

        }
        public override string Execute()
        {
            return this.ShowAllTeams();
        }

        private string ShowAllTeams()
        {
            if (this.Repository.Teams.Count > 0)
            {
                var sb = new StringBuilder();

                foreach (var team in this.Repository.Teams)
                {
                    sb.AppendLine(team.Name.ToString());

                }
                return sb.ToString().Trim();
            }
            else
            {
                return "There are no teams!";
            }
        }
    }
}
