﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Commands;
using Task_Management.Commands.Contracts;
using Task_Management.Core;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models;
using Task_Management.Models.Contracts;
namespace TaskManagment.Test.CommandsTest
{
    [TestClass]
    public class AddMemberToTeam_Should
    {
        IRepository repository;
        ICommandFactory commandFactory;
        ICommand createTeam;
        ICommand createBoard;
        ICommand createPerson;

        [TestInitialize]
        public void Setup()
        {
            repository = new Repository();
            commandFactory = new CommandFactory(repository);
            createTeam = commandFactory.Create("createteam Teams");
            createTeam.Execute();
            createBoard = commandFactory.Create("createboard Teams Board");
            createBoard.Execute();
            createPerson = commandFactory.Create("createPerson Pesho");
            createPerson.Execute();
        }
        [TestMethod]
        public void AddMemberToTeam_Should_Throw_When_AregumentAreNotTehRightNumber()
        {
            //Arrang
            string personName = "Pesho";
            //Act
            ICommand addPersonToATeam = commandFactory.Create($"addmember {personName}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => addPersonToATeam.Execute());
        }
        [TestMethod]
        public void AddMemberToTeam_Should_Throw_When_PersonIsNotValid()
        {
            //Arrang
            string teamName = "Teams";
            //Act
            ICommand addPersonToATeam = commandFactory.Create($"addmember Gosho {teamName}");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => addPersonToATeam.Execute());
        }
        [TestMethod]
        public void AddMemberToTeam_Should_Throw_When_TeamIsNotValid()
        {
            //Arrang
            string personName = "Pesho";
            //Act
            ICommand addPersonToATeam = commandFactory.Create($"addmember {personName} Kotaracite");
            //Assert
            Assert.ThrowsException<InvalidUserInputException>(() => addPersonToATeam.Execute());
        }
        [TestMethod]
        public void AddMemberToTeam_Should_Return_When_InputIsValid()
        {
            //Arrang
            string personName = "Pesho";
            string teamName = "Teams";
            //Act
            ICommand addPersonToATeam = commandFactory.Create($"addmember {personName} {teamName}");
            //Assert
            Assert.AreEqual("Person with name:Pesho was added in team:Teams", addPersonToATeam.Execute());
        }
    }
}
