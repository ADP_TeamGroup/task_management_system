﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;


namespace TaskManagment.Test.Models.Test.BugTest
{
    [TestClass]
    public class Steps_Should
    {
        PriorityType priority = PriorityType.Medium;
        SeverityType severity = SeverityType.Major;
        string step = "Open VS";
        int id = 5;
        [TestMethod]
        public void BugSteps_Should_ReturnCopy_When_AddsSteps()
        {
            //Arrange
            string title = "Fix that bug mate";
            string description = "Can not load in the matrix";
            
            Bug bug = new Bug(id,title, description,priority, severity,step) ;
            //Act
           

            //Assert
            Assert.AreEqual(1, bug.Steps.Count);
            Assert.AreEqual("Open VS", bug.Steps[0]);
        }
        
    }
}
