﻿using Task_Management.Models.Enums;

namespace Task_Management.Models.Contracts
{
    public interface IStory : ITask
    {
        public PriorityType Priority { get; set; }

        public SizeStory Size { get; set; }

        public StoryStatus Status { get; set; }


    }
}
