﻿using System.Collections.Generic;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;
using Task_Management.Models.Enums;

namespace Task_Management.Commands
{
    public class CreateStoryInBoardCommand : BaseCommand
    {
        public CreateStoryInBoardCommand(List<string> parameters, IRepository repository)
            : base(parameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != Constants.StoryMinCountParameters)
            {
                string errorMsg = string.Format(Constants.NumberOFArgumentsErrorMsg, Constants.StoryMinCountParameters, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMsg);
            }

            string teamName = this.CommandParameters[0];
            string boardName = this.CommandParameters[1];

            if (!this.Repository.BoardExists(boardName, teamName))
            {
                throw new InvalidUserInputException($"Board with name:{boardName} in team:{teamName} already exists!");
            }


            string title = this.CommandParameters[2];
            string description = this.CommandParameters[3];
            PriorityType priority = this.ParseEnumParameter<PriorityType>(this.CommandParameters[4], "priority");
            SizeStory size = this.ParseEnumParameter<SizeStory>(this.CommandParameters[5], "size");
            StoryStatus storyStatus = this.ParseEnumParameter<StoryStatus>(this.CommandParameters[6], "status");

            return CreateStory(teamName, boardName, title, description, priority, size, storyStatus);
        }

        private string CreateStory(string teamName, string boardName, string title, string description
                        , PriorityType priority, SizeStory size
                        , StoryStatus storyStatus)
        {
            var story = this.Repository.CreateStory(teamName, boardName, title, description
                        , priority, size, storyStatus);

            return $"Story with Id:{story.ID} was created in board:{boardName} in team:{teamName}!";
        }

    }
}
