﻿using Task_Management.Models.Enums;

namespace Task_Management.Models.Contracts
{
    public interface IFeedback : ITask
    {
        public int Rating { get; set; }

        public FeedbackStatus Status { get; set; }

    }
}
