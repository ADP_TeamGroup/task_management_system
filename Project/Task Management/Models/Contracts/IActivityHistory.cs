﻿
namespace Task_Management.Models
{
    public interface IActivityHistory
    {
        public string History { get; }
    }
}
