﻿using System.Collections.Generic;

namespace Task_Management.Models.Contracts
{
    public interface IMember : IHistory
    {
        public string Name { get; }

        public IList<ITask> Tasks { get; }

        public void AddTasksToMember(ITask task);

        public void RemoveTaskOfMember(ITask task);

        public void AddComment(IComment commentToAdd, ITask taskToAddComment);

        //TODO
        // activity history;
    }
}
