﻿using Task_Management.Models.Contracts;

namespace Task_Management.Models
{
    public class Comment : IComment
    {
        public Comment(string commentMsg, string member)
        {
            this.CommentMsg = commentMsg;
            this.Member = member;
        }

        public string CommentMsg { get; protected set; }

        public string Member { get; protected set; }
    }
}
