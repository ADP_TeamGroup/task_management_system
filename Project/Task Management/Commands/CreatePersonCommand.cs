﻿using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Core.Contracts;
using Task_Management.Exceptions;

namespace Task_Management.Commands
{
    public class CreatePersonCommand : BaseCommand
    {
        public CreatePersonCommand(IList<string> commandParameters, IRepository repository)
            : base(commandParameters, repository)
        {
        }

        public override string Execute()
        {
            if (this.CommandParameters.Count != 1)
            {
                string errorMassage = string.Format(Constants.NumberOFArgumentsErrorMsg, 1, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMassage);
            }

            string personName = this.CommandParameters[0];

            if (this.Repository.PersonExists(personName))
            {
                throw new InvalidUserInputException($"Person with name:{personName} already exists!");
            }

            return CreatePerson(personName);
        }

        private string CreatePerson(string personName)
        {
            this.Repository.CreatePerson(personName);

            return $"Person with name:{personName} was created!";
        }

    }
}