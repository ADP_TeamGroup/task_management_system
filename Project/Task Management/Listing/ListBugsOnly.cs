﻿using System.Text;
using Task_Management.Commands;
using Task_Management.Core.Contracts;
using System.Linq;
using System.Collections.Generic;
using Task_Management.Exceptions;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;

namespace Task_Management.Listing
{
    public class ListBugsOnly : BaseCommand
    {
        public ListBugsOnly(IList<string> parameters, IRepository repository)
            : base(parameters, repository) { }

        public override string Execute()
        {
            if (this.CommandParameters.Count > 4 || this.CommandParameters.Count < 3)
            {
                string errorMassage = string.Format(Constants.ListingNumberOfArgumentsErrorMsg, 3, 4, this.CommandParameters.Count);
                throw new InvalidUserInputException(errorMassage);
            }

            if (this.CommandParameters.Count == 3)
            {
                string filterBy = this.CommandParameters[0];
                string parameterForFilter = this.CommandParameters[1];
                string sortBy = this.CommandParameters[2];

                return ListBugsWithOneFilter(filterBy, parameterForFilter, sortBy);
            }

            return "";
        }

        private string ListBugsWithOneFilter(string filterBy,string parameterForFilter, string sortBy)
        {
            IList<IBug> listWithBugs = new List<IBug>();
            string print = string.Empty;

            switch (filterBy.ToLower())
            {
                case "status":
                    var parameter = ParseEnumParameter<BugStatus>(parameterForFilter, "status");

                    listWithBugs = (IList<IBug>)this.Repository.Tasks.Where(b => b is IBug).ToList();
                    listWithBugs = listWithBugs.Where(b => b.Status == parameter).ToList();
                    break;

                case "assignee":

                    listWithBugs = (IList<IBug>)this.Repository.MembersInTeam.Select(m => m.Tasks.Where(t => t is IBug)).ToList();
                    break;

                default:
                    throw new InvalidUserInputException("Invalid command! Please chose status or assignee!");
            }

            switch (sortBy.ToLower())
            {
                case "title": listWithBugs.OrderBy(t => t.Title); break;
                case "description": listWithBugs.OrderBy(t => t.Description); break;
                case "priority": listWithBugs.OrderBy(t => t.Priority); break;
                case "severity": listWithBugs.OrderBy(t => t.Severity); break;
                default:
                    throw new InvalidUserInputException($"Bug doesn't contain: {sortBy} to sort by!");

            }


            return PrintList(listWithBugs);
        }

        private string PrintList(IList<IBug> printList)
        {
            var sbBug = new StringBuilder();
            sbBug.AppendLine("Bugs:");

            foreach (var bugs in printList)
            {
                sbBug.AppendLine($"ID: {bugs.ID} : '{bugs.Title}'");
            }

            return sbBug.ToString().Trim();
        }
    }
}
