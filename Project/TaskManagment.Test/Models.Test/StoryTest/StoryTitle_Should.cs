﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Task_Management.Models.Contracts;
using Task_Management.Models.Enums;
using Task_Management.Models;
using Task_Management.Exceptions;


namespace TaskManagment.Test.Models.Test.StoryTest
{

    [TestClass]
    public class StoryTitle_Should
    {
        const PriorityType priority = PriorityType.Medium;
        const SizeStory size = SizeStory.Large;
        const StoryStatus storyStatus = StoryStatus.InProgress;

        const string description = "\"You can do it mate\"";
        const int id = 1;
        //string title, string description, PriorityType priority, SizeStory size, StoryStatus status
        [TestMethod]
        public void StoryTitle_Should_Throw_When_TitleIsNull()
        {

            Assert.ThrowsException<InvalidUserInputException>(() => new Story(id, null, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryTitle_Should_Throw_When_TitleIsWhiteSpace()
        {
            string title = new string(' ',9);

            Assert.ThrowsException<InvalidUserInputException>(() => new Story(id, title, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryTitle_Should_Throw_When_TitleLengthIsTooShort()
        {
            string title = new string('a', 4);

            Assert.ThrowsException<OutOfRangeException>(() => new Story(id, title, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryTitle_Should_Throw_When_TitleLengthIsTooLong()
        {
            string title = new string('a', Constants.StoryTitleMaxLength + 1);

            Assert.ThrowsException<OutOfRangeException>(() => new Story(id, title, description, priority, size, storyStatus));
        }
        [TestMethod]
        public void StoryTitle_Should_Return_When_InputIsValid()
        {
            string title = new string('a', Constants.StoryTitleMaxLength -1);
            Story story = new Story(id, title, description, priority, size, storyStatus);

            Assert.AreEqual(title,story.Title );
        }
    }
}
